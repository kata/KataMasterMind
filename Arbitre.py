from collections import defaultdict

class Arbitre(object):
    def __init__(self):
        self.right_place = 0

        self.colors_in_secret = defaultdict(int)
        self.colors_in_prop = defaultdict(int)

    def evaluate(self, secret, prop):

        for s, p in zip(secret, prop):
            print('s ' + s + ' p ' + p)
            self.count_pions(p, s)

        return [self.right_place, self.wrong_place()]

    def wrong_place(self):
        resultat = 0
        for key, value in self.colors_in_secret.items():
            resultat += min(value,self.colors_in_prop[key])

        return resultat

    def count_pions(self, propColor, secretColor):
        if secretColor == propColor:
            self.right_place += 1
        else:
            self.colors_in_secret[secretColor] += 1
            self.colors_in_prop[propColor] += 1

