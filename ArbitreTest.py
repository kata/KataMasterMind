import unittest

from Arbitre import Arbitre


class ArbitreTest(unittest.TestCase):

    def testToutFauxPourCombinaisonUnSeulElement(self):
        self.assertEqual(Arbitre().evaluate(["blue"], ["red"]), [0, 0])

    def testToutJustePourCombinaisonUnSeulElement(self):
        self.assertEqual(Arbitre().evaluate(["blue"], ["blue"]), [1, 0])
        self.assertEqual(Arbitre().evaluate(["red"], ["red"]), [1, 0])

    def testPlusieursElementsJustesSurCombinaisonPlusieursElements(self):
        self.assertEqual(Arbitre().evaluate(["blue", "red"], ["blue", "green"]), [1, 0])
        self.assertEqual(Arbitre().evaluate(["blue", "green"], ["red", "green"]), [1, 0])
        self.assertEqual(Arbitre().evaluate(["blue", "green", "yellow"], ["blue", "green", "red"]), [2, 0])


    def testQuelquesElementsMalPlacesParmiPlusieurs(self):
        self.assertEqual(Arbitre().evaluate(["blue","purple"],["purple","red"]),[0,1])
        self.assertEqual(Arbitre().evaluate(["blue", "blue", "purple", "purple"],
                                               ["purple", "purple", "green", "green"]), [0, 2])


    def testNeCompterLesMauvesQueSIlYEnADansLeSecret(self):
        self.assertEqual(Arbitre().evaluate(["blue","green"],["red","purple"]),[0,0])

    def testUnMalPlaceNeDoitEtreCompteQuUneFois(self):
        self.assertEqual(Arbitre().evaluate(["blue","blue","purple"],["purple","purple","red"]),[0,1])

    def testJauneAussiPeutEtreMalPlace(self):
        self.assertEqual(Arbitre().evaluate(["blue","blue","yellow"],["yellow","yellow","red"]),[0,1])

    def testNoirAussiPeutEtreMalPlace(self):
        self.assertEqual(Arbitre().evaluate(["blue","blue","black"],["black","black","red"]),[0,1])


    def testRougeAussiPeutEtreMalPlace(self):
        self.assertEqual(Arbitre().evaluate(["blue","blue","red"],["red","red","yellow"]),[0,1])

    def testToutesLesCouleursPeuventEtreMalPlacees(self):
        self.assertEqual(Arbitre().evaluate(["blue","red","yellow","green","purple","black","pink"], \
                                            ["red","yellow","green","purple","black","pink","blue"]),[0,7])

if __name__ == '__main__':
    unittest.main()
